#include <gtk/gtk.h>
#include "window.h"

static void
activate_cb (GtkApplication *app)
{
  GtkWindow *win = gtk_application_get_active_window (app);

  if (!win)
    win = my_window_new (app);

  gtk_window_present (win);
}

int
main (int    argc,
      char **argv)
{
  g_autoptr (GtkApplication) app = gtk_application_new ("org.example.App", G_APPLICATION_FLAGS_NONE);

  g_signal_connect (app, "activate", G_CALLBACK (activate_cb), NULL);

  return g_application_run (G_APPLICATION (app), argc, argv);
}
