#include "window.h"

/* Object */

#define MY_TYPE_OBJECT (my_object_get_type())

G_DECLARE_FINAL_TYPE (MyObject, my_object, MY, OBJECT, GObject)

struct _MyObject
{
  GObject parent_instance;

  guint number;
};

G_DEFINE_FINAL_TYPE (MyObject, my_object, G_TYPE_OBJECT)

static void
my_object_class_init (MyObjectClass *klass)
{
}

static void
my_object_init (MyObject *self)
{
}

MyObject *
my_object_new (guint number)
{
  MyObject *self = g_object_new (MY_TYPE_OBJECT, NULL);

  self->number = number;
  
  return self;
}

guint
my_object_get_number (MyObject *self)
{
  return self->number;
}



/* Model */

#define MY_TYPE_MODEL (my_model_get_type())

G_DECLARE_FINAL_TYPE (MyModel, my_model, MY, MODEL, GObject)

struct _MyModel
{
  GObject parent_instance;
};

static guint
my_model_get_n_items (GListModel *model)
{
  return 3;
}

static GType
my_model_get_item_type (GListModel *model)
{
  return MY_TYPE_OBJECT;
}

static gpointer
my_model_get_item (GListModel *model,
                   guint       pos)
{
  return my_object_new (pos);
}

static void
my_model_list_model_init (GListModelInterface *iface)
{
  iface->get_n_items = my_model_get_n_items;
  iface->get_item_type = my_model_get_item_type;
  iface->get_item = my_model_get_item;
}

G_DEFINE_FINAL_TYPE_WITH_CODE (MyModel, my_model, G_TYPE_OBJECT,
                               G_IMPLEMENT_INTERFACE (G_TYPE_LIST_MODEL, my_model_list_model_init))

static void
my_model_class_init (MyModelClass *klass)
{
}

static void
my_model_init (MyModel *self)
{
}



/* Window */

struct _MyWindow
{
  GtkApplicationWindow parent_instance;
};

G_DEFINE_FINAL_TYPE (MyWindow, my_window, GTK_TYPE_APPLICATION_WINDOW)

static char *
get_item_name (gpointer  data,
               MyObject *item)
{
  return g_strdup_printf ("Item %u", my_object_get_number (item));
}

static void
my_window_class_init (MyWindowClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/example/App/window.ui");
  gtk_widget_class_bind_template_callback (widget_class, get_item_name);
}

static void
my_window_init (MyWindow *self)
{
  g_type_ensure (MY_TYPE_MODEL);

  gtk_widget_init_template (GTK_WIDGET (self));
}

GtkWindow *
my_window_new (GtkApplication *app)
{
  return g_object_new (MY_TYPE_WINDOW, "application", app, NULL);
}
